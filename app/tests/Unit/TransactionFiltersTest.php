<?php


namespace Tests\Unit;


use Illuminate\Support\Carbon;

class TransactionFiltersTest extends ApiTester
{

    /** @test */
    function testItWillReturnCorrectPagination()
    {
        $this->makeUser();
        $make = 50;
        $this->times($make)->makeTransaction(['amount' => 1]);

        $response = $this->get($this->getAuthUrl('/api/v1/transactions', ['limit' => 3]));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount(3, $data);
    }

    /** @test */
    function testItWillReturnCorrectRecordWithAmountFrom()
    {
        $this->makeUser();
        $make = 5;
        $this->times($make)->makeTransaction(['amount' => 1]);
        $this->times($make * 2)->makeTransaction(['amount' => 10]);

        $response = $this->get($this->getAuthUrl('/api/v1/transactions', ['amount_from' => 1]));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount($make, $data);
    }

    /** @test */
    function testItWillReturnCorrectRecordWithAmountFromAndAmountTo()
    {
        $this->makeUser();
        $make = 5;
        $this->times($make)->makeTransaction(['amount' => 1]);
        $this->times($make * 2)->makeTransaction(['amount' => 5]);
        $this->times($make * 3)->makeTransaction(['amount' => 10]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/transactions',
            ['amount_from' => 1, 'amount_to' => 5, 'limit' => 99999] // ignore default pagination
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount($make * 3, $data);
    }

    /** @test */
    function testItWillReturnOnlyIncome()
    {
        $this->makeUser();
        $make = 5;
        $this->times($make * 2)->makeTransaction(['amount' => 1]);
        $this->times($make)->makeTransaction(['amount' => -5]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/transactions',
            ['limit' => 99999], // ignore default pagination
            ['income']
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount($make * 2, $data);
    }

    /** @test */
    function testItWillReturnOnlyExpense()
    {
        $this->makeUser();
        $make = 5;
        $this->times($make * 2)->makeTransaction(['amount' => 1]);
        $this->times($make)->makeTransaction(['amount' => -5]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/transactions',
            ['limit' => 99999], // ignore default pagination
            ['expense']
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount($make, $data);
    }

    /** @test */
    function testItWillReturnOnly1DayWithOnlyCreatedFrom()
    {
        $this->makeUser();
        $make = 10;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $this->times($make)->makeTransaction(['created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['created_at' => $dateNext, 'updated_at' => $dateNext]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/transactions',
            ['created_from' => $date->format('Y-m-d'), 'limit' => 99999] // ignore default pagination
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount($make, $data);
    }

    /** @test */
    function testItWillReturnWithCreatedFromAndCreatedTo()
    {
        $this->makeUser();
        $make = 10;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $dateFar = $date->copy()->addDays(30);
        $this->times($make)->makeTransaction(['created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['created_at' => $dateNext, 'updated_at' => $dateNext]);
        $this->times($make)->makeTransaction(['created_at' => $dateFar, 'updated_at' => $dateFar]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/transactions',
            [
                'created_from' => $date->format('Y-m-d'),
                'created_to'   => $dateNext->format('Y-m-d'),
                'limit'        => 99999,
            ] // ignore default pagination
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount($make * 2, $data);
    }
}