<?php


namespace Tests\Unit;


use Illuminate\Support\Carbon;

class StatisticControllerTest extends ApiTester
{
    /** @test */
    function testItCalculatesIncome()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $this->times($make)->makeTransaction(['amount' => $amount]);
        $this->times($make)->makeTransaction(['amount' => -$amount]);

        $response = $this->get($this->getAuthUrl('/api/v1/statistic/income'));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * $amount, $data);
    }

    /** @test */
    function testItCalculatesExpense()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $this->times($make)->makeTransaction(['amount' => $amount]);
        $this->times($make)->makeTransaction(['amount' => -$amount]);

        $response = $this->get($this->getAuthUrl('/api/v1/statistic/expense'));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * -$amount, $data);
    }

    /** @test */
    function testItCalculatesTotal()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $this->times($make)->makeTransaction(['amount' => $amount]);
        $this->times($make)->makeTransaction(['amount' => -$amount]);

        $response = $this->get($this->getAuthUrl('/api/v1/statistic/total'));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals(0, $data);
    }

    /** @test */
    function testItCalculatesIncomeFor1DayPeriod()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $dateNext, 'updated_at' => $dateNext]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/statistic/income',
            ['created_from' => $date->format('Y-m-d'), 'limit' => 99999]
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * $amount, $data);
    }

    /** @test */
    function testItCalculatesIncomeForPeriodOfDays()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $dateFar = $date->copy()->addDays(30);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $dateNext, 'updated_at' => $dateNext]);
        $this->times($make)->makeTransaction(['created_at' => $dateFar, 'updated_at' => $dateFar]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/statistic/income',
            [
                'created_from' => $date->format('Y-m-d'),
                'created_to'   => $dateNext->format('Y-m-d'),
                'limit'        => 99999,
            ]
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * $amount * 2, $data);
    }

    /** @test */
    function testItCalculatesExpenseFor1DayPeriod()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $this->times($make)->makeTransaction(['amount' => -$amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $dateNext, 'updated_at' => $dateNext]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/statistic/expense',
            ['created_from' => $date->format('Y-m-d'), 'limit' => 99999]
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * -$amount, $data);
    }

    /** @test */
    function testItCalculatesExpenceForPeriodOfDays()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $dateFar = $date->copy()->addDays(30);
        $this->times($make)->makeTransaction(['amount' => -$amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => -$amount, 'created_at' => $dateNext, 'updated_at' => $dateNext]);
        $this->times($make)->makeTransaction(['created_at' => $dateFar, 'updated_at' => $dateFar]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/statistic/expense',
            [
                'created_from' => $date->format('Y-m-d'),
                'created_to'   => $dateNext->format('Y-m-d'),
                'limit'        => 99999,
            ]
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * -$amount * 2, $data);
    }

    /** @test */
    function testItCalculatesTotalFor1DayPeriod()
    {
        $this->makeUser();
        $make = 10;
        $amount = 15;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => -$amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => -$amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $dateNext, 'updated_at' => $dateNext]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/statistic/total',
            ['created_from' => $date->format('Y-m-d'), 'limit' => 99999]
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * -$amount, $data);
    }

    /** @test */
    function testItCalculatesTotalForPeriodOfDays()
    {
        $this->makeUser();
        $make = 10;
        $amount = 10;
        $date = Carbon::create();
        $dateNext = $date->copy()->addDays(5);
        $dateFar = $date->copy()->addDays(30);
        $this->times($make)->makeTransaction(['amount' => -$amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => $amount, 'created_at' => $date, 'updated_at' => $date]);
        $this->times($make)->makeTransaction(['amount' => -$amount, 'created_at' => $dateNext, 'updated_at' => $dateNext]);
        $this->times($make)->makeTransaction(['created_at' => $dateFar, 'updated_at' => $dateFar]);

        $response = $this->get($this->getAuthUrl(
            '/api/v1/statistic/total',
            [
                'created_from' => $date->format('Y-m-d'),
                'created_to'   => $dateNext->format('Y-m-d'),
                'limit'        => 99999,
            ]
        ));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->result;
        $this->assertEquals($make * -$amount, $data);
    }
}