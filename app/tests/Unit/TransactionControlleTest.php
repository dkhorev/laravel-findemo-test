<?php

namespace Tests\Unit;


use App\Transaction;
use App\User;
use Illuminate\Http\Response;

class TransactionControlleTest extends ApiTester
{
    /** @test */
    public function testItPreventsAccessWithoutKey()
    {
        $this->makeUser();
        $this->makeTransaction();

        $response = $this->get('/api/v1/transactions');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function testItPreventsAccessWithBadKey()
    {
        $this->makeUser();
        $this->makeTransaction();

        $response = $this->get('/api/v1/transactions/?api_token=123');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function testItFetchesTransactions()
    {
        $this->makeUser();
        $make = 5;
        $this->times($make)->makeTransaction();

        $response = $this->get($this->getAuthUrl('/api/v1/transactions/'));

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertCount($make, $data);
    }

    /** @test */
    function testItFetchesASingleTransaction()
    {
        $this->makeUser();
        $this->makeTransaction();

        $response = $this->get($this->getAuthUrl('/api/v1/transactions/1/'));

        $response->assertOk();

        $data = json_decode($response->getContent())->data;
        $this->assertObjectHasAttribute('id', $data);
        $this->assertObjectHasAttribute('title', $data);
        $this->assertObjectHasAttribute('amount', $data);
        $this->assertObjectHasAttribute('author_id', $data);
    }

    /** @test */
    function testItWillReturn404OnTransactionNotFound()
    {
        $this->makeUser();
        $this->makeTransaction();

        $response = $this->get($this->getAuthUrl('/api/v1/transactions/999/'));

        $response->assertNotFound();
    }

    /** @test */
    function testItWillStoreNewTransaction()
    {
        $this->makeUser();

        $response = $this->post($this->getAuthUrl('/api/v1/transactions/'), [
            'title'     => 'test',
            'amount'    => 10,
            'author_id' => 1,
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        $data = json_decode($response->getContent())->data;
        $this->assertEquals('test', $data->title);
        $this->assertEquals(10, $data->amount);
        $this->assertEquals(1, $data->author_id);
    }

    /** @test */
    function testItRespondsWith422OnStoreFail()
    {
        $this->makeUser();

        $response = $this->post($this->getAuthUrl('/api/v1/transactions/'), []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    function testItWillDeleteExistingTransaction()
    {
        $this->makeUser();
        $this->makeTransaction();

        $response = $this->delete($this->getAuthUrl('/api/v1/transactions/1/'));

        $response->assertOk();
    }

    /** @test */
    function testItRespondsWith404OnDeleteTransactionNotFound()
    {
        $this->makeUser();

        $response = $this->delete($this->getAuthUrl('/api/v1/transactions/1/'));

        $response->assertNotFound();
    }

    /** @test */
    function testItWillNotReturnOtherUsersTransaction()
    {
        $this->makeUser(); // id user 1
        $this->makeUser(['api_token' => '123', 'email' => 'other@mail.ru']); // id user 2
        $this->makeTransaction(['author_id' => 1]); // id trans 1
        $this->makeTransaction(['author_id' => 2]); // id trans 2

        $response = $this->get('/api/v1/transactions/1/?api_token=123');

        $response->assertNotFound();
    }

    function testItWillNotShowOtherUsersTransactionsInList()
    {
        $this->makeUser(); // id user 1
        $this->makeUser(['api_token' => '123', 'email' => 'other@mail.ru']); // id user 2
        $this->makeTransaction(['author_id' => 1]); // id trans 1

        $response = $this->get('/api/v1/transactions/?api_token=123');

        $response->assertOk();

        $data = json_decode($response->getContent())->data->data;
        $this->assertEmpty($data);
    }

}
