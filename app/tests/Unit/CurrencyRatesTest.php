<?php

namespace Tests\Unit;

use App\DKDev\Facades\CurrencyRates;
use Tests\TestCase;

class CurrencyRatesTest extends TestCase
{
    /** @test */
    function testDefaultService()
    {
        $rate = CurrencyRates::get('EUR/USD');

        $this->assertEquals(1.15, $rate);
    }

    /** @test */
    function testDefaultServiceWithHelper()
    {
        $rate = currency_rates('EUR/USD');

        $this->assertEquals(1.15, $rate);
    }

    /** @test */
    function testXmlServiceWithHelper()
    {
        $rate = currency_rates('EUR/USD', 'xml');

        $this->assertEquals(1.25, $rate);
    }

    /** @test */
    function testCsvServiceWithHelper()
    {
        $rate = currency_rates('EUR/USD', 'csv');

        $this->assertEquals(1.35, $rate);
    }

    /** @test */
    function testAverageServiceWithHelper()
    {
        $rate = currency_rates('EUR/USD', 'average');

        $this->assertEquals((1.15 + 1.25 + 1.35) / 3, $rate);
    }

    /** @test */
    function testAverageServiceWithHelperUnknownPair()
    {
        $rate = currency_rates('EUR/RUB', 'average');

        $this->assertEquals(false, $rate);
    }

    /** @test */
    function testDefaultServiceUnknownPair()
    {
        $rate = CurrencyRates::get('EUR/RUB');

        $this->assertEquals(false, $rate);
    }

    /** @test */
    function testXmlService()
    {
        $rate = CurrencyRates::driver('xml')->get('EUR/USD');

        $this->assertEquals(1.25, $rate);
    }

    /** @test */
    function testXmlServiceUnknownPair()
    {
        $rate = CurrencyRates::driver('xml')->get('EUR/RUB');

        $this->assertEquals(false, $rate);
    }

    /** @test */
    function testCsvService()
    {
        $rate = CurrencyRates::driver('csv')->get('EUR/USD');

        $this->assertEquals(1.35, $rate);
    }

    /** @test */
    function testCsvServiceUnknownPair()
    {
        $rate = CurrencyRates::driver('csv')->get('EUR/RUB');

        $this->assertEquals(false, $rate);
    }

    /** @test */
    function testCanGetAverageRate()
    {
        $rate = CurrencyRates::average('EUR/USD');

        $this->assertEquals((1.15 + 1.25 + 1.35) / 3, $rate);
    }

    /** @test */
    function testCanGetAverageRateUnknownPair()
    {
        $rate = CurrencyRates::average('EUR/RUB');

        $this->assertEquals(false, $rate);
    }
}
