<?php


namespace Tests\Unit;


use App\Http\Middleware\VerifyCsrfToken;
use App\Transaction;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ApiTester extends TestCase
{
    // use RefreshDatabase;
    // use WithoutMiddleware;

    protected $faker;
    protected $times = 1;
    protected $apiKey = 'xxx111qqq';

    protected function noCsrf()
    {
        $this->withoutMiddleware(VerifyCsrfToken::class);
    }

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->faker = Faker::create();
    }

    public function getAuthUrl($url, $fields = [], $fieldsNoIndex= [])
    {
        $fields = array_merge(['api_token' => $this->apiKey], $fields);
        $query = http_build_query($fields);

        // http_build_query bug with no index fields
        if ($fieldsNoIndex) {
            $query .= '&' . implode('&', $fieldsNoIndex);
        }

        return $url . '?' . $query;
    }

    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate');

        $this->noCsrf();
    }

    public function times($n)
    {
        $this->times = $n;

        return $this;
    }

    public function makeTransaction($fields = [])
    {
        // $date = $this->faker->dateTimeBetween('-5 days', '+5 days');

        $transaction = array_merge([
            'title'     => $this->faker->sentence,
            'amount'    => $this->faker->numberBetween(10, 50),
            'author_id' => 1,
            // 'created_at' => $date,
            // 'updated_at' => $date,
        ], $fields);

        while ($this->times--) {
            Transaction::create($transaction);
        }

        $this->times = 1;
    }

    public function makeUser($fields = [])
    {
        $user = array_merge([
            'email'     => 'test@test.ru',
            'password'  => '123',
            'api_token' => $this->apiKey,
        ], $fields);

        while ($this->times--) {
            User::create($user);
        }

        $this->times = 1;
    }
}