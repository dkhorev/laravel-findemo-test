<?php

use App\DKDev\Facades\CurrencyRates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('register', 'Auth\RegisterController@register');

Route::middleware(['api_key'])->group(function () {
    Route::group(['prefix' => 'api/v1/statistic'], function () {
        Route::get('income', 'Api\StatisticController@income');
        Route::get('expense', 'Api\StatisticController@expense');
        Route::get('total/{pair?}/{driver?}', 'Api\StatisticController@total');
    });

    Route::group(['prefix' => 'api/v1'], function () {
        Route::resource('transactions', 'Api\TransactionController')
             ->only(['index', 'store', 'show', 'destroy']);
    });
});
