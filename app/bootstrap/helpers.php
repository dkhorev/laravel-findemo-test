<?php

if (! function_exists('currency_rates')) {
    /**
     * Get currency pair.
     *
     * @param string $pair
     * @param string $driver
     *
     * @return mixed|\Illuminate\Cache\CacheManager
     *
     * @throws Exception
     */
    function currency_rates(string $pair, string $driver = '')
    {
        $arguments = func_get_args();

        if (!$driver) {
            return app('CurrencyRates')->get($pair);
        }

        if ($driver === 'average') {
            return app('CurrencyRates')->average($pair);
        }

        return app('CurrencyRates')->driver($driver)->get($pair);
    }
}
