@extends('layouts.app')

@section('content')
    <div class="container">
        New transaction added. ID: {{ $transaction->id }}
    </div>
@endsection
