<?php

return [
    'bad_key'   => 'Bad api key.',
    'error_505' => 'Internal error.',
    'error_401' => 'Not authorized.',
    'error_404' => 'Not found.',
];
