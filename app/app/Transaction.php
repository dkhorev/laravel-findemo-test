<?php

namespace App;

use App\DKDev\Filters\QueryFilter;
use App\DKDev\Filters\TransactionFilters;
use App\DKDev\Scopes\UserAccessScope;
use App\Events\NotifyTransactionAdded;
use App\Mail\TransactionAdded;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Requests\TransactionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class Transaction
 *
 * @package App
 *
 * @method static Builder filter(QueryFilter $filters, $customFilters = [])
 */
class Transaction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'amount', 'author_id', 'created_at', 'updated_at'
    ];

    /**
     * @param TransactionRequest $request
     *
     * @return mixed
     */
    public static function storeFromApi(TransactionRequest $request)
    {
        $user = User::byApiToken($request)->first();

        $data = array_merge(
            $request->only(['title', 'amount']),
            ['author_id' => $user->id]
        );

        /* @var Transaction $transaction */
        $transaction = Transaction::create($data);

        if ($transaction) {
            Log::info('New mail add to queue: ', $transaction->toArray());
            Mail::queue(new TransactionAdded($transaction));
            event(new NotifyTransactionAdded($transaction));
        }

        return $transaction;
    }

    /**
     * @param Request            $request
     * @param TransactionFilters $filters
     *
     * @return mixed
     */
    public static function getList(Request $request, TransactionFilters $filters)
    {
        $limit = $request->limit ? : config('api.default_pagination');

        $transactions = Transaction::filter($filters)->paginate($limit);

        return $transactions->toArray();
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserAccessScope);
    }

    /**
     * @param             $query
     * @param QueryFilter $filters
     *
     * @param array       $customFilters
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public static function scopeFilter($query, QueryFilter $filters, $customFilters = [])
    {
        return $filters->apply($query, $customFilters);
    }

    /**
     * @param QueryFilter $filters
     *
     * @return mixed
     */
    public static function getIncome(QueryFilter $filters)
    {
        return static::getStatisticSumByType($filters, ['income']);
    }

    /**
     * @param $filters
     *
     * @return mixed
     */
    public static function getExpense($filters)
    {
        return static::getStatisticSumByType($filters, ['expense']);
    }

    /**
     * @param $filters
     *
     * @return mixed
     */
    public static function getTotal($filters)
    {
        return static::getStatisticSumByType($filters);
    }

    /**
     * @param       $filters
     * @param array $fields
     *
     * @return mixed
     */
    protected static function getStatisticSumByType($filters, $fields = [])
    {
        $transaction = Transaction::filter($filters, $fields);

        return $transaction->sum('amount');
    }

    /**
     * Get transaction's author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }
}
