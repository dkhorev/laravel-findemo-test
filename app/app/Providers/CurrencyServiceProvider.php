<?php

namespace App\Providers;

use App\DKDev\CurrencyRates\CurrencyRates;
use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('CurrencyRates', function ($app) {
            $cache = $app->config->get('currency.cache');
            $repository = $app['cache']->store($cache);

            return new CurrencyRates(config('currency'), $repository->getStore());
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['CurrencyRates'];
    }
}
