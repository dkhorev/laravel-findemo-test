<?php


namespace App\DKDev\CurrencyRates;


use Illuminate\Contracts\Cache\Store;

/**
 * Class CurrencyRates
 *
 * @package App\DKDev\CurrencyRates
 */
class CurrencyRates
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var Store
     */
    protected $store;

    private $driver;
    private $cache_ttl;
    private $services;

    /**
     * CurrencyRates constructor.
     *
     * @param array $config
     * @param Store $store
     */
    public function __construct(array $config, Store $store)
    {
        $this->config = $config;
        $this->store = $store;

        $this->driver = $this->config['default'];
        $this->cache_ttl = $this->config['cache_ttl'];
    }

    /**
     * @param string $pair
     *
     * @return mixed
     * @throws \Exception
     */
    public function get(string $pair)
    {
        $serviceName = $this->getDriver();
        $cacheKey = $this->getCacheKey($pair);

        $cachedRate = $this->store->get($cacheKey);

        if ($cachedRate) {
            return $cachedRate;
        }

        $serviceClass = $this->getServiceName();

        if (!class_exists($serviceClass)) {
            throw new \Exception("Service {$serviceClass} does not exist.");
        }

        if (!isset($this->services[$serviceName])) {
            $this->services[$serviceName] = new $serviceClass($this->config['services'][$serviceName]);
        }

        /** @var CurrencyService $service */
        $service = $this->services[$serviceName];

        $rate = $service->get($pair);

        $this->store->put($cacheKey, $rate, $this->cache_ttl);

        return $rate;
    }

    /**
     * @param string $driver
     *
     * @return CurrencyRates $this
     */
    public function driver(string $driver)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * @param string $pair
     *
     * @return float
     * @throws \Exception
     */
    public function average(string $pair)
    {
        $averageRate = 0;
        $availableServices = 0;

        foreach ($this->config['services'] as $service => $config) {
            $rate = $this->driver($service)->get($pair);

            if ($rate) {
                $averageRate += $rate;
                $availableServices++;
            }

        }

        if ($availableServices) {
            return $averageRate / $availableServices;
        }

        return false;
    }

    /**
     * @param string $pair
     */
    public function resetCache(string $pair)
    {
        foreach ($this->config['services'] as $service => $config) {
            $cacheKey = $this->getCacheKey($pair);
            $this->store->forget($cacheKey);
        }
    }

    /**
     * @return mixed
     */
    private function getDriver()
    {
        return $this->driver;
    }

    /**
     * @return string
     */
    private function getServiceName()
    {
        return 'App\\DKDev\\CurrencyRates\\' . ucwords($this->getDriver()) . 'Service';
    }

    /**
     * @param string $pair
     *
     * @return string
     */
    private function getCacheKey(string $pair)
    {
        return 'CurrencyRates:' . $this->getDriver() . ':' . $pair;
    }
}