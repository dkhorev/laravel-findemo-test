<?php


namespace App\DKDev\CurrencyRates;

class CsvService extends CurrencyService
{
    /**
     * @param string $pair
     *
     * @return mixed
     */
    protected function loadRate(string $pair)
    {
        $data = file($this->config['path']);

        foreach ($data as $line){
            list($linePair, $rate) = explode(';', $line);

            if ($linePair === $pair) {
                return (float) $rate;
            }
        }

        return false;
    }
}