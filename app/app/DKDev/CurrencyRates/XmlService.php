<?php


namespace App\DKDev\CurrencyRates;

class XmlService extends CurrencyService
{
    /**
     * @param string $pair
     *
     * @return mixed
     */
    protected function loadRate(string $pair)
    {
        $data = simplexml_load_file($this->config['path']);

        foreach ($data->pair as $xmlPair) {
            if ((string)$xmlPair->name === $pair) {
                return (float)$xmlPair->rate;
            }
        }

        return false;
    }
}