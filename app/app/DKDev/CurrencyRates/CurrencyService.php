<?php


namespace App\DKDev\CurrencyRates;


abstract class CurrencyService
{
    /**
     * @var array
     */
    protected $config;

    /**
     * CurrencyService constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $pair
     *
     * @return mixed
     * @throws \Exception
     */
    public function get(string $pair)
    {
        $rate = $this->loadRate($pair);

        return $rate;
    }

    /**
     * @param string $pair
     *
     * @return bool|\Psr\Http\Message\ResponseInterface|void
     * @throws \Exception
     */
    protected function loadRate(string $pair)
    {
        throw new \Exception('Implement loadRate() for ' . static::class);
    }
}