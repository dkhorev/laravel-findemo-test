<?php


namespace App\DKDev\CurrencyRates;

class JsonService extends CurrencyService
{
    /**
     * @param string $pair
     *
     * @return mixed
     */
    protected function loadRate(string $pair)
    {
        $data = file_get_contents($this->config['path']);
        $data = json_decode($data);

        $rate = array_filter($data, function ($item) use ($pair) {
            return $item->pair === $pair;
        });

        if ($rate) {
            $rate = array_pop($rate);

            return $rate->rate;
        }

        return false;
    }
}