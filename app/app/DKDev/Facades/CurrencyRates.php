<?php


namespace App\DKDev\Facades;

use App\DKDev\CurrencyRates\CurrencyRates as CurrencyRatesClass;
use Illuminate\Support\Facades\Facade;

/**
 * Class CurrencyRates
 *
 * @method static array get(string $pair)
 * @method static CurrencyRatesClass driver(string $driver)
 * @method static array average(string $pair)
 * @method static void resetCache(string $pair)
 *
 * @package App\DKDev\Facades
 */
class CurrencyRates extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'CurrencyRates';
    }
}