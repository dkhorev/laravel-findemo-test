<?php


namespace App\DKDev\Converters;


/**
 * Class TransactionConverter
 *
 * @package App\DKDev\Converters
 */
class CurrencyConverter extends Converter
{

    /**
     * @param $params
     *
     * @return int
     */
    public function convert($params)
    {
        $value = $params['value'];
        $pair = $params['pair'];
        $driver = $params['driver'] ?: config('currency.default');

        try {
            $rate = currency_rates($this->getFullPair($pair), $driver);

            return (float) $value * $rate;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $pair
     *
     * @return string
     */
    private function getFullPair($pair)
    {
        return $pair .'/'. config('currency.default_currency');
    }
}