<?php


namespace App\DKDev\Converters;


/**
 * Class TransactionConverter
 *
 * @package App\DKDev\Converters
 */
class TransactionConverter extends Converter
{
    /**
     * @param array $transaction
     *
     * @return array
     */
    public function convert($transaction)
    {
        return [
            'id'         => $transaction['id'],
            'title'      => $transaction['title'],
            'amount'     => $transaction['amount'],
            'author_id'  => $transaction['author_id'],
            'created_at' => $transaction['created_at'],
            'updated_at' => $transaction['updated_at'],
        ];
    }
}