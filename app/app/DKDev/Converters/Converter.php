<?php


namespace App\DKDev\Converters;


/**
 * Class Converter
 *
 * @package App\DKDev\Converters
 */
abstract class Converter
{
    /**
     * @param array $items
     *
     * @return array
     */
    public function convertCollection($items)
    {
        return array_map([$this, 'convert'], $items);
    }

    /**
     * @param array $item
     *
     * @return array
     */
    abstract public function convert($item);
}