<?php


namespace App\DKDev\Filters;


use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class QueryFilter
 *
 * @package App\DKDev\Filters
 */
abstract class QueryFilter
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Builder
     */
    protected $builder;

    /**
     * QueryFilter constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param Builder $builder
     *
     * @param array   $customFilters
     *
     * @return Builder
     */
    public function apply(Builder $builder, $customFilters = [])
    {
        $this->builder = $builder;

        $arFilters = $this->filters();
        foreach ($customFilters as $filter => $val) {
            if ($filter) {
                $arFilters[$filter] = $val;
            } else {
                $arFilters[$val] = '';
            }
        }

        foreach ($arFilters as $name => $value) {
            if (method_exists($this, $name)) {
                call_user_func_array([$this, $name], array_filter([$value]));
            }
        }

        return $this->builder;
    }

    /**
     * @return array
     */
    public function filters()
    {
        return $this->request->all();
    }


}