<?php


namespace App\DKDev\Filters;


use Illuminate\Support\Carbon;

/**
 * Class TransactionFilters
 *
 * @package App\DKDev\Filters
 */
class TransactionFilters extends QueryFilter
{
    /**
     * @param      $amount_from
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function amount_from($amount_from)
    {
        if ($this->request->has('amount_to')) {
            return $this->builder->whereBetween('amount', [$amount_from, $this->request->amount_to]);
        }

        return $this->builder->where('amount', $amount_from);
    }

    /**
     * @param $created_from
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function created_from($created_from)
    {
        $dateFrom = Carbon::parse($created_from);
        $dateTo = $dateFrom->copy()->addDay();

        if ($this->request->has('created_to')) {
            $dateTo = Carbon::parse($this->request->created_to)
                            ->addDay();
        }

        return $this->builder->whereBetween('created_at', [
            $dateFrom->toDateString(),
            $dateTo->toDateString()
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function income()
    {
        return $this->builder->where('amount', '>', 0);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function expense()
    {
        return $this->builder->where('amount', '<', 0);
    }


}