<?php


namespace App\DKDev\Cron;


use App\DKDev\Facades\CurrencyRates;

class UpdateCurrencyRates
{
    /**
     * @throws \Exception
     */
    public function __invoke()
    {
        CurrencyRates::resetCache('EUR/USD');
        currency_rates('EUR/USD', 'average');
    }
}