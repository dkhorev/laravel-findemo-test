<?php

namespace App\Http\Controllers\Api;


use App\DKDev\Converters\CurrencyConverter;
use App\DKDev\Filters\TransactionFilters;
use App\Transaction;

/**
 * Class StatisticController
 *
 * @package App\Http\Controllers\Api
 */
class StatisticController extends ApiController
{
    /**
     * @param TransactionFilters $filters
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function income(TransactionFilters $filters)
    {
        $result = Transaction::getIncome($filters);

        return $this->respondOk(['result' => $result]);
    }


    /**
     * @param TransactionFilters $filters
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function expense(TransactionFilters $filters)
    {
        $result = Transaction::getExpense($filters);

        return $this->respondOk(['result' => $result]);
    }

    /**
     * @param TransactionFilters $filters
     *
     * @param string             $pair
     * @param string             $driver
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function total(TransactionFilters $filters, string $pair = '', string $driver ='')
    {
        $result = Transaction::getTotal($filters);

        if ($pair) {
            $result = (new CurrencyConverter())->convert([
                'value'  => $result,
                'pair'   => $pair,
                'driver' => $driver,
            ]);
        }

        return $this->respondOk(['result' => $result]);
    }
}
