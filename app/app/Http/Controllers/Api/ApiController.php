<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
abstract class ApiController extends Controller
{
    /**
     * @var int
     */
    protected $statusCode = Response::HTTP_OK;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }


    /**
     * @param $statusCode
     *
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function responNotFound($message = '')
    {
        if (!$message) {
            $message = __('app.error_404');
        }

        return $this->setStatusCode(Response::HTTP_NOT_FOUND)
                    ->respondWithError($message);
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function responUnauthorized($message = '')
    {
        if (!$message) {
            $message = __('app.error_401');
        }

        return $this->setStatusCode(Response::HTTP_UNAUTHORIZED)
                    ->respondWithError($message);
    }

    /**
     * @param       $data
     * @param array $headers
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respond($data, $headers = [])
    {
        return response($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param $message
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'error'       => true,
            'status_code' => $this->getStatusCode(),
            'message'     => $message,
        ]);
    }

    /**
     * @param $data
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondOk($data)
    {
        return $this->respond([
            'error'       => false,
            'status_code' => $this->getStatusCode(),
            'data'        => $data,
        ]);
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondInternalError($message = '')
    {
        if (!$message) {
            $message = __('app.error_500');
        }

        return $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                    ->respondWithError($message);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    protected function getUser(Request $request)
    {
        return User::byApiToken($request)->first();
    }
}
