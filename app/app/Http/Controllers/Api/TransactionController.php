<?php

namespace App\Http\Controllers\Api;

use App\DKDev\Converters\TransactionConverter;
use App\DKDev\Filters\TransactionFilters;
use App\Http\Requests\TransactionRequest;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class TransactionController
 *
 * @package App\Http\Controllers
 */
class TransactionController extends ApiController
{
    /**
     * @var TransactionConverter
     */
    protected $converter;

    /**
     * TransactionController constructor.
     *
     * @param TransactionConverter $converter
     */
    public function __construct(TransactionConverter $converter)
    {
        $this->converter = $converter;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request            $request
     *
     * @param TransactionFilters $filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, TransactionFilters $filters)
    {
        $data = Transaction::getList($request, $filters);
        $data['data'] = $this->converter->convertCollection($data['data']);

        return $this->respondOk($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TransactionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {
        $transaction = Transaction::storeFromApi($request);

        return $this->setStatusCode(Response::HTTP_CREATED)
                    ->respondOk($this->converter->convert($transaction->toArray()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int    $id
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::find($id);

        if (!$transaction) {
            return $this->responNotFound();
        }

        return $this->respondOk($this->converter->convert($transaction->toArray()));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int     $id
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::find($id);

        if (!$transaction) {
            return $this->responNotFound();
        }

        try {
            $transaction->delete();
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }

        return $this->respondOk(['status' => 'deleted']);
    }

}
