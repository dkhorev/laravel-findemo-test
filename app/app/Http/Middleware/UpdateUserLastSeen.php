<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Carbon;

class UpdateUserLastSeen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::byApiToken($request)->first();

        if ($user) {
            $user->last_seen = Carbon::create();
            $user->save();
        }

        return $next($request);
    }
}
