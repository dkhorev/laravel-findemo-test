<?php

namespace App\Http\Middleware;

use App\Transaction;
use App\User;
use Closure;
use Illuminate\Http\Response;

class ApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::byApiToken($request)->first();

        if (!$user || !$request->api_token) {
            return response()->json([
                'error'   => true,
                'message' => __('app.bad_key'),
            ], Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
