<?php

return [
    /**
     * default currency driver
     */
    'default' => env('CURRENCY_SERVICE_DEFAULT', 'json'),

    /**
     * default currency of application (what values are stored in DB)
     */
    'default_currency' => env('CURRENCY_SERVICE_DEFAULT_CURRENCY', 'USD'),

    /**
     * default currency cache driver
     */
    'cache' => env('CURRENCY_SERVICE_CACHE', 'file'),

    /**
     * default currency cache TTL
     */
    'cache_ttl' => env('CURRENCY_SERVICE_CACHE_TTL', 5), // minutes

    /**
     * all available services
     */
    'services' => [
        'xml'  => ['path' => storage_path('app/public/rates.xml')],
        'json' => ['path' => storage_path('app/public/rates.json')],
        'csv'  => ['path' => storage_path('app/public/rates.csv')],
    ],
];
