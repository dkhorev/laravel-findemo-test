<?php

return [
    'default_pagination'      => env('API_DEFAULT_PAGINATION', 5),
];