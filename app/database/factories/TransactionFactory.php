<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Transaction::class, function (Faker $faker) {

    $date = $faker->dateTimeBetween('-5 days', '+5 days');

    return [
        'title'     => $faker->sentence,
        'amount'    => random_int(-100, 100),
        'author_id' => $faker->randomElement(\App\User::all())->id,
        'created_at' => $date,
        'updated_at' => $date,
    ];
});
