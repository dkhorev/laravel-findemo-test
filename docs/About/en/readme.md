1) How SOLID is used in your project

    a) Micro-level of classes and methods

Single responsibility \
[/app/app/Http/Controllers/Api/StatisticController.php](/app/app/Http/Controllers/Api/StatisticController.php) \
All events are made as SRP classe (well, this is standard in Laravel)

Open-closed principle \
[SRP + OCP in currency rates converter](/app/app/DKDev/CurrencyRates) \
[SRP + OCP in data converter](/app/app/DKDev/Converters)

Liskov substitution \
[in CurrencyRates module - all services can substitute each other](/app/app/DKDev/CurrencyRates)

Interface segregation \
[In this project only from Laravel itself - event realizes broadcast only when it needs to + traits upgrade object's capabilities without need of implementing stub methods for all other objects](/app/app/Events/NotifyTransactionAdded.php)

Dependency inversion \
[Constructor here accepts Store interface and it doesn't depend on any specific caching method](/app/app/DKDev/CurrencyRates/CurrencyRates.php)

   b) Macro-level, architecture

On architecture level it is made by Laravel itself - dependency injection by typehinting, class containers, event system and so on.

2) What is good use of PHPDoc? Give evaples in your code.

When everything is documented IDE doesn't highlight unknow props or methods, but instead shows hints as you type.

Correct phpdoc block make navigation in code very easy. Easy to see what each method accepts and returns.

We can generate API documentation automatically.


Examples: \
[Facade of converter has methods in class description](/app/app/DKDev/Facades/CurrencyRates.php) \
[Modle has its scopes descriptions](/app/app/Transaction.php)


3) What did you like about PHP 7+?

Faster than 5.6 \
Typehinting for scalar types \
Typehinting of return values


4) What do you think about typehinting in PHP 7?

It makes code more stable as you have type-control in your methods. Also IDE shows potential errors of type mismatch instantly.

It makes working with IDE much easier.

I think one shoul use typehinting for any new projects, made on modern technology stack. An use with care when working with older libraries (or make typehinted adapters for them :)