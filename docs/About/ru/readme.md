1) Как в Вашем проекте используются принципы SOLID

    a) В микро-масштабах на уровне кода классов

Single responsibility \
[/app/app/Http/Controllers/Api/StatisticController.php](/app/app/Http/Controllers/Api/StatisticController.php) \
Все события реализованы через SRP классы (вобщем-то это стандарт для Laravel)

Open-closed principle \
[SRP + OCP в конвертере валют](/app/app/DKDev/CurrencyRates) \
[SRP + OCP в конвертере данных](/app/app/DKDev/Converters)

Liskov substitution \
[Самый яркий пример тут конечно конвертер валют - все сервисы взаимозаменяемы](/app/app/DKDev/CurrencyRates)

Interface segregation \
[Только на примере из самого laravel - событие реализует вещание только когда это нужно ему + трейты как бы дополняют функциональность объекта без необходимости тянуть их со всеми наследниками](/app/app/Events/NotifyTransactionAdded.php)

Dependency inversion \
[Конструктор в этом классе примиает не конкретный кеш-класс, а контракт и поэтому не зависит от  реализации, кеш модуля легко заменяется в настройках](/app/app/DKDev/CurrencyRates/CurrencyRates.php)

   b) В макро-масштабе на уровне архитектуры системы

На уровне архитектуры это используется в самом Laravel - dependency injection по требованию, контейнеры сервисов, событийная система и т.д.

2) Что по Вашему означает правильное использование PHPDoc? Приведите примеры в Вашем коде.

Когда все описано и IDE не ругается на неизвестные св-ва, методы, классы и т.п., а наоборот дает подсказки во время набора.

Легко просмотреть, что треубется классу/методу, что вернется. \
Можно оставить для IDE подсказки в описании класса, если методы "магические" и обычным способом IDE их никак не найдет. \
Возможность описать св-ва модели, чтобы IDE не ругалась (в этом проекте не делал). \
Можно сгенерить документацию автоматически (никогда не делал).

Примеры в коде: \
[Фасад для конвертера валют имеет в описании класса доступные методы](/app/app/DKDev/Facades/CurrencyRates.php) \
[Модель через описание класса дает возможность увидеть scopes определенные в модели](/app/app/Transaction.php)


3) Что Вам понравилось или не понравилось в PHP 7+?

Быстрее 5.6 \
Typehinting для простых типов переменных помимо array \
Объявление значений которые вернутся из метода


4) Как вы относитесь к типизации в PHP 7? Когда стоит использовать, когда нет? На Ваш взгляд...

Положительно. Облегчает работу с IDE и требование определенных типов данных делают код более устойчивым.

Думаю нужно пользоваться всегда при разработке модулей "с нуля" и пользоваться с осторожность при работе с чужими, особенно старыми, библиотеками или API (либо пропускать их через адаптеры, которые реализуют строгую типизацию).