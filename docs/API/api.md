### Transactions API

#### GET /api/v1/transactions/

Get list of transactions with or without additional filters.

| Parameter | Description|
| -------- | -------- |
| api_token   | Only required param. Or bad auth response.   |
| limit   | Results pagination. Default number is in .env<br> Example: /api/v1/transactions/?limit=3   |
| income   | Show only positive amount transactions<br> No value needed<br> Example: /api/v1/transactions/?income  |
| expense   | Show only negative amount transactions<br> No value needed<br> Example: /api/v1/transactions/?expense   |
| amount_from | Filter by amount exactly.<br>If amount_to is not set - select only transaction amount === amount_from<br>Example: /api/v1/transactions/?amount_from=10<br>(only amount === 10 transactions are returned) |
| amount_to | Filter by amount to. Must provide amout_from for this to work. Filter by range [amount_from, amount_to]<br>Example: /api/v1/transactions/?amount_from=10&amount_to=100<br>( 10 <= amount <= 100 transactions are returned) |
| created_from | Filter by created date exactly.<br>If created_to is not set - select only transaction created_at this date<br> Example: /api/v1/transactions/?created_from=28.10.2018 |
| created_to | Filter by created date. Must provide created_from for this to work. Filter by range [created_from, created_to]<br>Example: /api/v1/transactions/?created_from=28.10.2018&created_to=30.10.2018 |

#### GET /api/v1/transactions/{id}/

Get a transaction by ID

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| {id}   | ID of user owned transaction   |

#### POST /api/v1/transactions/

Create a transaction

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| title   | Transaction title   |
| amount   | Transaction amount   |

#### DELETE /api/v1/transactions/{id}/

Delete a transaction

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| {id}   | ID of user owned transaction   |



### Statistic API

#### GET /api/v1/statistic/income/

Get all positive amount for transactions for a period (period is optional).

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| created_from | Filter by created date exactly.<br>If created_to is not set - select only transaction created_at this date<br> Example: /api/v1/income/?created_from=28.10.2018 |
| created_to | Filter by created date. Must provide created_from for this to work. Filter by range [created_from, created_to]<br>Example: /api/v1/income/?created_from=28.10.2018&created_to=30.10.2018 |

Response:
```
{
    "error": false,
    "status_code": 200,
    "data": {
        "result": "5249"
    }
}
```

#### GET /api/v1/statistic/expense/

Get all negative amount for transactions for a period (period is optional).

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| created_from | Filter by created date exactly.<br>If created_to is not set - select only transaction created_at this date<br> Example: /api/v1/expense/?created_from=28.10.2018 |
| created_to | Filter by created date. Must provide created_from for this to work. Filter by range [created_from, created_to]<br>Example: /api/v1/expense/?created_from=28.10.2018&created_to=30.10.2018 |

Response:
```
{
    "error": false,
    "status_code": 200,
    "data": {
        "result": "-4733"
    }
}
```

#### GET /api/v1/statistic/total/{pair?}/{driver?}/

Get total amount for transactions for a period (period is optional).

| Route option | Description|
| -------- | -------- |
| pair   | Convert result to currency<br>Example: /api/v1/statistic/total/EUR/  |
| driver   | Convert result to currency using driver<br>Example: /api/v1/statistic/total/EUR/average/<br>Example: /api/v1/statistic/total/EUR/xml/  |

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| created_from | Filter by created date exactly.<br>If created_to is not set - select only transaction created_at this date<br> Example: /api/v1/total/?created_from=28.10.2018 |
| created_to | Filter by created date. Must provide created_from for this to work. Filter by range [created_from, created_to]<br>Example: /api/v1/total/?created_from=28.10.2018&created_to=30.10.2018 |

Response:
```
{
    "error": false,
    "status_code": 200,
    "data": {
        "result": "516"
    }
}
```