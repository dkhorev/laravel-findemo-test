cd ..

sudo -- sh -c "echo 127.0.0.1 findemo.loc >> /etc/hosts"

cp .env.example .env \
(this is docker specific .env)

In .env file change MYSQL_ROOT_PASSWORD to anything you like.

docker build -t findemo-php ./docker/php/

docker-compose up -d

Wait a few minutes for it to pull and build all containers.

Go to http://findemo.loc:8888 for adminer

Login and make a database for demo project.
```
server=db
user=root
password=<MYSQL_ROOT_PASSWORD>
```

cd ./app

Fill DB_DATABASE and DB_PASSWORD properties in Laravel .env file.

```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=<your database name>
DB_USERNAME=root
DB_PASSWORD=<db root pass from .env MYSQL_ROOT_PASSWORD>
```

Bash into app container: \
docker exec -it findemoloc_app_1 bash

php artisan migrate