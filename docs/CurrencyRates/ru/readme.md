# Конвертер валют

[Тесты дают хорошее представление о возможностях модуля](/app/tests/Unit/CurrencyRatesTest.php)

## Конфигурация

В файле [/app/config/currency.php](/app/config/currency.php)

```
    /**
     * default currency driver
     */
    'default' => env('CURRENCY_SERVICE_DEFAULT', 'json'),

    /**
     * default currency of application (what values are stored in DB)
     */
    'default_currency' => env('CURRENCY_SERVICE_DEFAULT_CURRENCY', 'USD'),

    /**
     * default currency cache driver
     */
    'cache' => env('CURRENCY_SERVICE_CACHE', 'file'),

    /**
     * default currency cache TTL
     */
    'cache_ttl' => env('CURRENCY_SERVICE_CACHE_TTL', 5), // minutes

    /**
     * all available services
     */
    'services' => [
        'xml'  => ['path' => storage_path('app/public/rates.xml')],
        'json' => ['path' => storage_path('app/public/rates.json')],
        'csv'  => ['path' => storage_path('app/public/rates.csv')],
    ],
```

## Facade

| Метод | Описание |
| -------- | -------- |
| CurrencyRates::get('EUR/USD');   | Вернет курс для пары с драйвера по умолчанию. Автоматически кешируется по конфигу. <b>False</b> если пара не будет найдена в указанном драйвере |
| CurrencyRates::driver('xml')->get('EUR/USD');   | Вернет курс для пары с конкрентого драйвера |
| CurrencyRates::average('EUR/USD');   | Вернет средний курс пары по всем драйверам (кто отдал значение) |
| CurrencyRates::resetCache('EUR/USD');    | сброс кеша по паре |

## Helper

| Метод | Описание |
| -------- | -------- |
| currency_rates('EUR/USD');   | Вернет курс для пары с драйвера по умолчанию или <b>false</b> если пара не будет найдена в указанном драйвере |
| currency_rates('EUR/USD', 'xml');   | Вернет курс для пары с конкрентого драйвера |
| currency_rates('EUR/USD', 'average');  | Вернет средний курс пары по всем драйверам (кто отдал значение) |

## Проверка баланса

Роут тот же что получает баланс, с опциональными параметрами.

#### GET /api/v1/statistic/total/{pair?}/{driver?}/

Get total amount for transactions for a period (period is optional).

| Route option | Description|
| -------- | -------- |
| pair   | Convert result to currency<br>Example: /api/v1/statistic/total/EUR/  |
| driver   | Convert result to currency using driver<br>Example: /api/v1/statistic/total/EUR/average/<br>Example: /api/v1/statistic/total/EUR/xml/  |

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| created_from | Filter by created date exactly.<br>If created_to is not set - select only transaction created_at this date<br> Example: /api/v1/total/?created_from=28.10.2018 |
| created_to | Filter by created date. Must provide created_from for this to work. Filter by range [created_from, created_to]<br>Example: /api/v1/total/?created_from=28.10.2018&created_to=30.10.2018 |

Response:
```
{
    "error": false,
    "status_code": 200,
    "data": {
        "result": "516"
    }
}
```