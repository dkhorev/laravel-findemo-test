# Currency converter

[See tests for global overview of module capabilities](/app/tests/Unit/CurrencyRatesTest.php)

## Configuration

In file [/app/config/currency.php](/app/config/currency.php)

```
    /**
     * default currency driver
     */
    'default' => env('CURRENCY_SERVICE_DEFAULT', 'json'),

    /**
     * default currency of application (what values are stored in DB)
     */
    'default_currency' => env('CURRENCY_SERVICE_DEFAULT_CURRENCY', 'USD'),

    /**
     * default currency cache driver
     */
    'cache' => env('CURRENCY_SERVICE_CACHE', 'file'),

    /**
     * default currency cache TTL
     */
    'cache_ttl' => env('CURRENCY_SERVICE_CACHE_TTL', 5), // minutes

    /**
     * all available services
     */
    'services' => [
        'xml'  => ['path' => storage_path('app/public/rates.xml')],
        'json' => ['path' => storage_path('app/public/rates.json')],
        'csv'  => ['path' => storage_path('app/public/rates.csv')],
    ],
```

## Facade

| Method | Description |
| -------- | -------- |
| CurrencyRates::get('EUR/USD');   | Returns rate from default driver. Automatic caching by config. <b>False</b> if pair does not exist in default driver. |
| CurrencyRates::driver('xml')->get('EUR/USD');   | Returns rate from specific driver |
| CurrencyRates::average('EUR/USD');   | Returns average rate from all drivers (if they have it) |
| CurrencyRates::resetCache('EUR/USD');    | Reset pair cache |

## Helper

| Method | Description |
| -------- | -------- |
| currency_rates('EUR/USD');   | Returns rate from default driver. Automatic caching by config. <b>False</b> if pair does not exist in default driver. |
| currency_rates('EUR/USD', 'xml');   | Returns rate from specific driver |
| currency_rates('EUR/USD', 'average');  | Returns average rate from all drivers (if they have it) |

## Balance check

Same route as for balance with optional parameters.

#### GET /api/v1/statistic/total/{pair?}/{driver?}/

Get total amount for transactions for a period (period is optional).

| Route option | Description|
| -------- | -------- |
| pair   | Convert result to currency<br>Example: /api/v1/statistic/total/EUR/  |
| driver   | Convert result to currency using driver<br>Example: /api/v1/statistic/total/EUR/average/<br>Example: /api/v1/statistic/total/EUR/xml/  |

| Parameter | Description|
| -------- | -------- |
| api_token   | Required param. Or bad auth response.   |
| created_from | Filter by created date exactly.<br>If created_to is not set - select only transaction created_at this date<br> Example: /api/v1/total/?created_from=28.10.2018 |
| created_to | Filter by created date. Must provide created_from for this to work. Filter by range [created_from, created_to]<br>Example: /api/v1/total/?created_from=28.10.2018&created_to=30.10.2018 |

Response:
```
{
    "error": false,
    "status_code": 200,
    "data": {
        "result": "516"
    }
}
```