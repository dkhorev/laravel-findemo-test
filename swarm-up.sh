#!/usr/bin/env bash

docker build -t findemo-php ./docker/php/
docker pull nginx:alpine
docker pull percona
docker pull adminer

docker swarm init

env $(cat .env | grep ^[A-Z] | xargs) docker stack deploy --with-registry-auth --compose-file docker-compose.yml findemo

ip addr show | grep 192.168.