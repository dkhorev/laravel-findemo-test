# Laravel Personal Finance Api Demo

[Task description RU](/docs/Task/ru/) \
[Task description EN](/docs/Task/en/)

[Сопроводительное письмо](/docs/About/ru/) \
[Project cover letter](/docs/About/en/)

[Описаниe Конвертера валют](/docs/CurrencyRates/ru/) \
[Currency converter description](/docs/CurrencyRates/en/)

# Installation

git clone https://gitlab.com/dkhorev/sample-php-laravel-personal-finance-api-2018 findemo.loc

cd findemo.loc/app

### Laravel setup

cp .env.example .env

composer install

php artisan key:generate

### Server - Homestead setup

[See instructions here](/docs/Install/homestead.md)

### Server - Docker setup (tested @Linux)

[See instructions here](/docs/Install/docker.md)

### Register a user to get api_token

Go to http://findemo.loc/register, register new user

```
You are logged in!

API token: 51aSk1eqr8Ed2p9ssBcphr3evv29UFZlLB2VX6Rangt5Drswuzxi4znvaVjm
```

Seed transactions (in ssh@homsted or app container): \
php artisan db:seed

All done!

## Tests

Located in ./app/tests/Unit/

Test are provided for:
```
TransactionController
StatisticController
TransactionFilters
CurrencyRates service
```

## API documentation

You can import Postman API testing collection form [/docs/Postman/](/docs/Postman/) \
Change api_token in all queries to your new user's token.

[API docs](/docs/API/api.md)




